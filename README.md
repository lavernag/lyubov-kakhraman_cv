# Lyubov Kakhraman_CV
## Hello, I'm Lyubov :wave:

![  ](images/rsz_photo01.jpg)



![  ](images/imgonline-com-ua-Resize-v5rMXKkaT6goO7.jpg)  
![  ](images/imgonline-com-ua-Resize-4nC5wYzCE9LkOeyP.jpg)  
+7 (927) 5418460  
![  ](images/imgonline-com-ua-Resize-7VloU5d434Guki3.jpg)
l777kahraman@gmail.com

Junior QA Engineer with 4 months experience in manual testing
I have finished School 21 QA enfineers courses and Stepik on-line courses on SQL, API and Python

## :star: Key Points
* Functional Testing
* UI- Testing
* Mobile Testing

## :hammer_and_wrench: Technical stack 
* TestIt
* Postman
* DevTools
* Gitlab

## :books: My opensource projects
https://gitlab.com/lavernag/lyubov-kakhraman_cv/-/tree/main/Rocket%20Chat%20Manual%20Testing

https://gitlab.com/lavernag/lyubov-kakhraman_cv/-/tree/main/Tutu%20Mobile%20Testing%20Native%20vs%20Web

<a href="https://ru.freepik.com/search?format=search&last_filter=query&last_value=telegram&query=telegram&type=icon">Источник иконки: Pixel perfect</a>
