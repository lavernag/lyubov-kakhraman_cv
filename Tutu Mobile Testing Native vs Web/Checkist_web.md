## Отчет о тестировании WEB версии мобильного приложения tutu

|Номер|Название|Результат|Комментарий|
|-----|--------|---------|-----------|
|6|Открытие приложения|Успешный|Поиск приложения через браузер,всплывающее окно с предожением перейти в приложение|
|7|Закрытие приложения|Успешный|Через браузер|
|8|Открытие приложения при текущем исходящем звонке|Успешный|-----|
|26|Просмотр заказов без подключения к сети|Провальный|Серый экран, "Нет подключения к интернету"|
|27|Просмотр списка доступных авиабилетов  без подключения к сети |Провальный|Серый экран, "Нет подключения к интернету"|
|31|Смена обложки|Провальный|Недоступно|
|32|Просмотр баннера в формате сториз|Провальный|Недоступно|
|37|Получение списка всех возможных рейсов и отелей по выбранному направлению|Провальный|Мжно выбирать только одну категорию рейсов|
|38|Просмотр раздела "Куда можно поехать"|Успешный|Перейти по ссылке" Открытие границ стран: какиедокументы куда нужны" в разделе "Главное" |
|3|Вход в существующий аккаунт по email и пароль|Успешный|-----|
|11|Выход из аккаунта|Успешный|-----|
|4|Попытка авторизации по неверному паролю|Провальный|Наличие кнопки "Быстрый вход без пароля"|
|9|Просмотр информации о выполненном заказе|Успешный|-----|
|10|Просмотр  данных о пассажире|Провальный|Нет такой опции|
|11|Просмотр уведомления|Провальный|-----|
|12|Отправка отзыва|Успешный|Отыв отправляется без перехода в ящик электронной почты|
|13|Изменение языка приложения на английский|Успешный|Перевод возможен при переходе на полную версию сайта. переводится интерфейс, статьи не переводятся|
|14|Просмотр информации о возврате билетов на поезд|Успешный| Возможен в разделе "Частые вопросы"            |
|15|Отправка сообщения в чате|Провальный| Недоступно, отправить сообщение можно через форму обратной связи|
|16|Отправка файла в чате|Провальный| Недоступно, отправить сообщение можно через форму обратной связи, функционала для прикрепления файлов нет            |
|17|Отправка сообщения в чате Telegram|Успешный|             |
|18|Получение списка доступных  авиабилетов по выбранным параметрам|Успешный|Наличие кнопок "Завтра", "Послезавтра", "Узнать расписание и цены"             |
|19|Бронирование отеля|Успешный|Кнопка называетс "Заказ гостиниц"             |
|20|Получение списка отелей для бронирования по выбранным параметрам|Успешный|             |
|21|Фильтрация  списка доступных  авиабилетов по выбранным параметрам|Успешный|             |
|22|Покупка авиабилетов по выбранным параметрам|Успешный|             |
|23|Получение списка  жд билетов по выбранным параметрам|Успешный|             |
|24|Покупка  жд билетов по выбранным параметрам|Успешный|             |
|25|Покупка авиабилетов с пустыми полями данных|Провальный|             |
|29|Получение списка   билетов на автобус по выбранным параметрам|Успешный|             |
|30|Покупка  билетов на автобус по выбранным параметрам|Успешный|             |
|33|Добавление услуги страхования при покупке  билетов на автобус по выбранным параметрам|Успешный|             |
|34|Покупка  билетов на автобус с вводом некорректной даты рождения пассажира|Провальный|Сообщение красным шрифтом "Вы правда старше 120 лет?:)"             |
|35|Просмотр информации о маршруте перед покупкой билета на автобус|Провальный|Недоступно             |
|36|Просмотр отзывов об автобусе перед покупкой билета на автобус|Провальный| Недоступно       |
|30|Покупка  билетов на автобус по выбранным параметрам|Успешный|             |
